import Vue from 'vue';

// Custom Vue filters
Vue.filter('money', function (val) {
    return (!isNaN(val)) ? val.toFixed(2) : val;
});

Vue.filter('format_int', function (val) {
    return (!isNaN(val)) ? Math.abs(val).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") : val;
});