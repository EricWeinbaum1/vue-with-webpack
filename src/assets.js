import Vue from 'vue';

import DropdownMenu from './components/Partials/DropdownMenu';
Vue.component('dropdown-menu', DropdownMenu);

import TextInput from './components/Partials/TextInput';
Vue.component('text-input', TextInput);

import CheckboxInput from './components/Partials/CheckboxInput';
Vue.component('checkbox-input', CheckboxInput);


