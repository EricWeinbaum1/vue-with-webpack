require('!!script!jquery');
require('!!script!jquery.cookie');

// Import files
import Vue            from 'vue';
import VueRouter      from 'vue-router';
import App            from './App';

// Enable router plugin
Vue.use(VueRouter);

var router = new VueRouter({
    hashbang: false,
    history: true
});

var getComponentResolve = function(component) {
    return function(resolve) {
        require(['./components/' + component], resolve);
    }
};

var parseRoutes = function(array) {
    var object = {};

    $.each(array, function() {
        object[this.route] = {
            component: getComponentResolve(this.component)
        };

        if (this.sub) {
            object[this.route].subRoutes = parseRoutes(this.sub)
        }
    });

    return object;
};

var routes_json = require("./routes.json");
var routes = parseRoutes(routes_json);

router.beforeEach(function (transition) {
    var from = transition.from.path || '[direct load]';
    var to = transition.to.path || '[unknown]';

    // Verify permitted transition
    if (isTransitionAllowed(to, from)) {
        console.info('%cTransitioning from ' + from + ' to ' + to, 'color: #7b08da');
        transition.next();

    } else {
        console.warn('%cProhibited transition from ' + from + ' to ' + to, 'color: #a81c46');
        router.go(from);

        if (from === '[direct load]') {
            router.redirect('/');
        }

        // Abort transition
        transition.abort();
    }
});

router.map(routes).start(App, 'app');
window.router = router;




/*
 * Fires before any page load (transition)
 */
function isTransitionAllowed(to, from) {

    // Abort if something went haywire
    if (typeof(to) == 'undefined') transition.abort();

    // Home page should always be permitted
    if (to === '/') {
        return true;
    }

    // Get a clean array of route hierarchy
    var route_clean = to.replace(/\//g, " ").trim().split(' ');
    route_clean.push('');

    // Get array of authorized routes
    var authorized = getAuthorizedAccess();
    if (authorized.indexOf(to) > -1) {
        console.info('%cAuthorized transition by cookie.', 'color: #2c8071');
        return true;
    }

    // Get allowed referrers
    var getAllowedReferrers = function(data, path) {
        return jQuery.map(data, function(r) {
            if (r.route == ('/' + path[0])) {
                if (r.sub && r.sub.length) {
                    path.shift();
                    return getAllowedReferrers(r.sub, path);
                } else if (r.allowed_referrers) {
                    return (r.allowed_referrers);
                }
            }
        });
    };

    var allowed_referrers = getAllowedReferrers(routes_json, route_clean);

    if ((allowed_referrers.length === 0) || allowed_referrers.indexOf(from) > -1) {
        return true;
    }

    return false;
}

function getAuthorizedAccess() {
    if (jQuery.cookie('cbl_auth')) {
        var data = JSON.parse(jQuery.cookie('cbl_auth'));
        
        if (data.authorized.length) {
            return data.authorized;
        }
    }

    return [];
}

