// Global scripts, defined in webpack.base.conf.js
require('!!script!jquery');
require('!!script!jquery.cookie');

// Expose jQuery instance
window.$ = jQuery;

// Import files
import Vue       from 'vue';
import VueRouter from 'vue-router';
import App       from './App';
import router    from './router';
import filters   from './filters';
import assets    from './assets';

// Enable Vue debug mode
Vue.config.debug = true;



