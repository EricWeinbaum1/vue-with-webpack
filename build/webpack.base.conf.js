var path = require('path');
var webpack = require('webpack');

module.exports = {

  entry: {
    app: './src/main.js'
  },

  output: {
    path: path.resolve(__dirname, '../dist/static'),
    publicPath: '/static/',
    filename: '[name].js'
  },

  resolve: {
    extensions: ['', '.js', '.vue'],
    alias: {
      'src': path.resolve(__dirname, '../src'),
      'jquery': 'jquery/dist/jquery'
    }
  },

  resolveLoader: {
    root: path.join(__dirname, 'node_modules')
  },

  module: {
    loaders: [
      {
        test: /\.vue$/,
        loader: 'vue'
      },
      {
        test: /\.js$/,
        // loader: 'babel!eslint',
        loader: 'babel',
        exclude: /node_modules/
      },
      {
        test: /\.json$/,
        loader: 'json'
      },
      {
        test: /\.(png|jpg|gif|ico|svg|woff|woff2|eot|ttf|php)$/,
        loader: 'url',
        query: {
          limit: 10000,
          name: '[name].[ext]?[hash:7]'
        }
      }
    ]
  },

  vue: {
    loaders: {
      // js: 'babel!eslint'
      js: 'babel'
    }
  },

  eslint: {
    formatter: require('eslint-friendly-formatter')
  }
}


