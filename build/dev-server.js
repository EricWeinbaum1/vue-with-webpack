var express = require('express')
var webpack = require('webpack')
var config = require('./webpack.dev.conf')
var app = express()
var compiler = webpack(config)

// handle fallback for HTML5 history API
app.use(require('connect-history-api-fallback')())

// serve webpack bundle output
app.use(require('webpack-dev-middleware')(compiler, {
  publicPath: config.output.publicPath,
  stats: {
    colors: true,
    chunks: false
  }
}))

// enable hot-reload and state-preserving
// compilation error display
app.use(require('webpack-hot-middleware')(compiler))

app.all('/ajax-mock.php', function (req, res) {
  res.json({'success': true})
})

app.post('/get-loan-options', function (req, res, next) {
  var loan_options = [
    {
      'term': 36,
      'rate': 9.17,
      'apr': 11.26,
      'offers': [
        { 'loan_id': 1, 'loan_amount': 5555, 'monthly_payment': 177.09 },
        { 'loan_id': 2, 'loan_amount': 6110, 'monthly_payment': 194.78 },
        { 'loan_id': 3, 'loan_amount': 6666, 'monthly_payment': 212.50 }
      ]
    },
    {
      'term': 60,
      'rate': 10.46,
      'apr': 14.04,
      'offers': [
        { 'loan_id': 4, 'loan_amount': 5555, 'monthly_payment': 127.33 },
        { 'loan_id': 5, 'loan_amount': 6110, 'monthly_payment': 140.05 },
        { 'loan_id': 6, 'loan_amount': 6666, 'monthly_payment': 152.80 }
      ]
    }
  ];

  res.json(loan_options)
  next()
})

app.listen(8080, 'localhost', function (err) {
  if (err) {
    console.log(err)
    return
  }
  console.log('Listening at http://localhost:8080')
})












