/* global describe, it, expect */

import Vue from 'vue';
import App from 'src/App';

describe('App.vue', () => {
  it('should render correct contents', () => {
    const vm = new Vue({
      template: '<div><main-header></main-header></div>',
      components: { MainHeader, MainFooter, LoadingSpinner }
    }).$mount();

    // expect(vm.$el.querySelector('.hello h1').textContent).toBe('Hello World!');
    
  })
})

// also see example testing a component with mocks at
// https://github.com/vuejs/vue-loader-example/blob/master/test/unit/a.spec.js#L24-L49
